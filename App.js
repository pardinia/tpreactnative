import React from "react";
import { StyleSheet, View } from "react-native";
import Navigation from './navigation/Navigation'
//https://material.io/tools/icons/?icon=assessment&style=baseline

export default class App extends React.Component {
  render() {
    return (
      <Navigation />
    );
  }
}
