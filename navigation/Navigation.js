import React from 'react'
import { createStackNavigator, createAppContainer, createBottomTabNavigator } from 'react-navigation'
import { StyleSheet, Image, Button } from 'react-native';
import Service from "../components/Service";
import Form from "../components/Form";
import Result from "../components/Result";
import ServiceItem from "../components/ServiceItem";
import Team from "../components/Team";

const SearchStackNavigator = createStackNavigator({
  Service: { // On peut mettre le nom que l'on souhaite
    screen: Service,
    navigationOptions: {
      title: 'Service',
      headerRight: (
        <Button
          onPress={() => alert('React Native Device Info ne fonctionne pas :(')}
          title="Info"
          color="black"
        />
      ),
    }
  },
  Form: {
    screen: Form,
    navigationOptions: {
      title: 'Formulaire',
      headerRight: (
        <Button
          onPress={() => alert('React Native Device Info ne fonctionne pas :(')}
          title="Info"
          color="black"
        />
      ),
    }
  },
  Result: {
    screen: Result,
    navigationOptions: {
      title: 'Membre',
      headerRight: (
        <Button
          onPress={() => alert('React Native Device Info ne fonctionne pas :(')}
          title="Info"
          color="black"
        />
      ),
    }
  }
})

const TeamStackNavigator = createStackNavigator({
  Team: { // On peut mettre le nom que l'on souhaite
    screen: Team,
    navigationOptions: {
      title: 'Team',
      headerRight: (
        <Button
          onPress={() => alert('React Native Device Info ne fonctionne pas :(')}
          title="Info"
          color="black"
        />
      ),
    }
  }
})

const MoviesTabNavigator = createBottomTabNavigator({
  Service: {
    screen: SearchStackNavigator,
    navigationOptions: {
      tabBarIcon: () => {
        return <Image
          source={require('../assets/icon/service.png')}
          style={styles.icon}/>
      }
    }
  },
  Team: {
    screen: TeamStackNavigator,
    navigationOptions: {
      tabBarIcon: () => {
        return <Image
          source={require('../assets/icon/developpeur.png')}
          style={styles.icon}/>
      }
    }
  }
},
  {
    tabBarOptions: {
      activeBackgroundColor: '#DDDDDD', // Couleur d'arrière-plan de l'onglet sélectionné
      inactiveBackgroundColor: '#FFFFFF', // Couleur d'arrière-plan des onglets non sélectionnés
      showLabel: false, // On masque les titres
      showIcon: true // On informe le TabNavigator qu'on souhaite afficher les icônes définis
    }
  }
)

const styles = StyleSheet.create({
  icon: {
    width: 35,
    height: 25
  }
})

export default createAppContainer(MoviesTabNavigator)
