import React from "react";
import { StyleSheet, View, FlatList, Text } from "react-native";

class ResultItem extends React.Component {
  render() {
    const user = this.props.user;
    const title = this.props.title;

    if (user.newsletter) {
      // on ne peut pas affiché la valeur "true" donc on fait une convertion en string
      newsletter = "oui";
    }

    if (title == "Spotify") {
      return (
        <View style={styles.main_container}>
          <View style={styles.global}>
            <View style={styles.nom_prenom}>
              <Text style={ styles.text_style }>{user.Prénom} {user.Nom}</Text>
            </View>
            <View style={styles.ville}>
              <Text style={styles.text_style_type}>Ville : </Text>
              <Text>{user.Ville}</Text>
            </View>
            <View style={styles.abonnement}>
              <Text style={styles.text_style_type}>Abonnement : </Text>
              <Text>{user.abonnement}</Text>
            </View>
            <View style={styles.abonnement}>
              <Text style={styles.text_style_type}>Newsletter : </Text>
              <Text>{newsletter}</Text>
            </View>
            <View style={styles.abonnement}>
              <Text style={styles.text_style_type}>Style : </Text>
              <Text>{user.Style}</Text>
            </View>
          </View>
        </View>
      );
    }
    // sinon le service est netflix
    return (
      <View style={styles.main_container}>
        <View style={styles.global}>
          <View style={styles.nom_prenom}>
            <Text style={ styles.text_style }>{user.Prénom} {user.Nom}</Text>
          </View>
          <View style={styles.info}>
            <Text style={styles.text_style_type}>Sexe : </Text>
            <Text>{user.info}</Text>
          </View>
          <View style={styles.ville}>
            <Text style={styles.text_style_type}>Ville : </Text>
            <Text>{user.Ville}</Text>
          </View>
          <View style={styles.abonnement}>
            <Text style={styles.text_style_type}>Abonnement : </Text>
            <Text>{user.abonnement}</Text>
          </View>
          <View style={styles.abonnement}>
            <Text style={styles.text_style_type}>Newsletter : </Text>
            <Text>{newsletter}</Text>
          </View>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    margin: 5,
    backgroundColor: "#FFFFFF",
  },
  global: {
    height: 170,
    backgroundColor: '#FFFFFF',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  nom_prenom: {
    flex: 1,
    margin: 5
  },
  ville: {
    flexDirection: "row",
    flex: 1,
    margin: 5
  },
  abonnement: {
    flexDirection: "row",
    flex: 1,
    margin: 5
  },
  info: {
    flex: 1,
    margin: 5,
    flexDirection: "row"
  },
  text_style: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  text_style_type: {
    fontWeight: 'bold',
  }
});

export default ResultItem;
