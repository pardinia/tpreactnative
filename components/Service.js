import React from "react";
import { StyleSheet, View, FlatList } from "react-native";
import services from "../assets/service.json";
import ServiceItem from "./ServiceItem";
import firebase from 'firebase';

class Service extends React.Component {
  // Constuire nos services
  constructor(props) {
    super(props);
    _services = []; // Déclaration tableau
    _services = services.services; // Insertion des services
  }

  componentWillMount() {
      // configuration de l'API pour firebase
      var config = {
        apiKey: "AIzaSyCuC7najPE8id9u4u9M9gk5aMHZrZ-puPw",
        authDomain: "reactnativedatabase-c8d7d.firebaseapp.com",
        databaseURL: "https://reactnativedatabase-c8d7d.firebaseio.com",
        projectId: "reactnativedatabase-c8d7d",
        storageBucket: "reactnativedatabase-c8d7d.appspot.com",
        messagingSenderId: "776401687040"
      };
      // initialisation de la connexion
      firebase.initializeApp(config);
    }

  _displayDetailForMember = (title, elements) => {
    this.props.navigation.navigate("Form", {title: title, elements: elements});
  }

  _affichResult = (title) => {
    this.props.navigation.navigate("Result", {title: title});
  }

  render() {
    return (
      <View style={styles.main_container}>
        <FlatList
          data={_services}
          keyExtractor={item => item.title} // définiton d'un clé unique par service
          renderItem={({ item }) =>
            <ServiceItem service={item}
              displayDetailForMember={this._displayDetailForMember}
              affichResult={this._affichResult} />}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
  }
});

export default Service;
