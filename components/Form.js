import React from "react";
import { AppRegistry } from 'react-native';
import { Alert, StyleSheet, View, Text, Image, TouchableOpacity, Button } from "react-native";
import GenerateForm from 'react-native-form-builder';
import firebase from 'firebase';

function element(type, required, label, name, section) { // Fonction pour construire un élément du formulaire
  if(label == "Prénom" || label == "Nom"){
    var construct = {"type":type,"required":required,"label":label,"name":name,"icon":"ios-person"};
    if(required == 'true'){
     _mandatory.push(label)
    }
  }
  else if(label == "Ville"){
    var construct = {"type":type,"required":required,"label":label,"name":name,"icon":"ios-home"};
    if(required == 'true'){
     _mandatory.push(label)
    }
  }
  else if(type == "picker"){
    var construct = {"type":type,"required":required,"label":section,"name":name,"options":label};
    if(required == 'true'){
     _mandatory.push(section)
    }
  }
  else if(type == "switch"){
    label = Boolean(label);
    var construct = {"type":type,"required":required,"label":section,"name":name,"defaultValue":label};
    if(required == 'true'){
     _mandatory.push(section)
    }
  }
  else{
    var construct = {"type":type,"required":required,"label":label,"name":name,"icon":null};
    if(required == 'true'){
     _mandatory.push(label)
    }
  }
  return construct;
}

class Form extends React.Component {
  constructor(props) {
    super(props);
    // Constuire les éléments
    this.state = {
          erreur: "" // Initialisation de notre donnée erreur dans le state
        }
    _mandatory = [];
    _elements = []; // déclaration tableau
    _elements = this.props.navigation.state.params.elements; // récupération des éléments
    _form = [];
    _button = [];
    _title = this.props.navigation.state.params.title;

    for(var i=0; i < _elements.length; i++){

      if(_elements[i].type == "edit"){
        var monElement = element("text", _elements[i].mandatory, _elements[i].value[0], _elements[i].value[0], null);
        _form.push(monElement);
      }
      if(_elements[i].type == "radioGroup"){

        var monElement = element("picker", _elements[i].mandatory, _elements[i].value, _elements[i].section, _elements[i].section);
        _form.push(monElement);
      }
      if(_elements[i].type == "switch"){
        var monElement = element("switch", _elements[i].mandatory, _elements[i].value[0], _elements[i].section, _elements[i].section);
        _form.push(monElement);
      }
      if(_elements[i].type == "button"){
        _button.push(_elements[i].value[0]);
        var mandatory = _elements[i].mandatory;
        var section = _elements[i].section;
      }
    }
    if(_button.length != 0){
      var monElement = element("picker", mandatory, _button, section, section);
      _form.push(monElement);
    }
  }

  login() {
    const formValues = this.formGenerator.getValues();

    // Ajout à la base de données les informations en fonction du service renseigné
     firebase.database().ref('users/' + _title).push(
         this.formGenerator.getValues()
     ).then(() => {
        // navigation sur la page des résultats après soumission du formulaire
        // passage en paramètre du titre du service
         this.props.navigation.navigate("Result", {title: _title});
     }).catch((error) => {
         console.log(error);
     });
  }

 validate(){
   const formValues = this.formGenerator.getValues(); // Récupération des données du formulaire
   var textErreur = "Remplissez : "
   var erreur = false;

   for(i=0; i < _mandatory.length; i++){ // Parcours des champs à remplir
    if(formValues[_mandatory[i]] == ""){
      erreur = true;
      textErreur = textErreur +" "+_mandatory[i]
    }
   }

   if(erreur == true){ // changement du state des erreurs
     this.setState({ erreur: textErreur })
     Alert.alert("Erreur","Veuillez consulter les erreurs")
   }else{
     this.login()
   }
 }

  render() {
    const {service, displayDetailForFilm} = this.props;

    return (
    <View>
      <View>
      <Text style={styles.text_style}>{this.state.erreur}</Text>
        <GenerateForm
          ref={(c) => {
            this.formGenerator = c;
          }}
          fields={_form}
        />
      </View>
      <View style={styles.submitButton}>
          <Button block onPress={() => this.validate()} title="VALIDER" />
      </View>
    </View>

    );
  }
}
  const styles = {
  submitButton: {
    paddingHorizontal: 10,
    paddingTop: 20
  },
  text_style: {
    margin: 5,
    fontWeight: 'bold',
    color: 'red',
    fontSize: 15
  }
};
export default Form;
