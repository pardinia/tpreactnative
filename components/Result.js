import React from "react";
import { StyleSheet, View, FlatList, Text, ActivityIndicator } from "react-native";
import firebase from 'firebase';
import ResultItem from './ResultItem';

class Result extends React.Component {
  // Constuire nos services
  constructor(props, title) {
    super(props, title);
    // récupération du service courant
    _title = this.props.navigation.state.params.title;
    _users = [];
    this.state = {
      users: [],
      loading: true
    };
  }

  // composant qui est lancé à chaque compilation de la vue
  componentDidMount() {
      firebase
        .database()
        .ref()
        .child('users/'+_title)
        .once('value')
        .then(data => {
          // parcourt des valeurs
          data.forEach(child => {
            // recupération des données sous forme de JSON
            var user = child.toJSON();
            _users.push(user)
          })
          this.setState({users: _users, loading: false})
        })
        .catch(error => {
          console.error(error);
        });
  }

  render() {
    // animation du loading pendant la récupération des données
    if (this.state.loading) {
      return (
        <ActivityIndicator
          animating={true}
          style={styles.indicator}
          size="large"
        />
      );
    }

    // si pas de données pour le service
    if (this.state.users.length == 0) {
      return (
        <View style={styles.main_container}>
          <Text>Pas de données</Text>
        </View>
      );
    }

    return (
      <View style={styles.main_container}>
        <FlatList
          data={this.state.users}
          keyExtractor={(item) => item.Nom}
          renderItem={({item}) => <ResultItem user={item} title={_title} />}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
  //  marginTop: 27
  },
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  }
});

export default Result;
