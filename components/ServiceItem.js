import React from "react";
import { StyleSheet, View, Text, Image, TouchableOpacity, Button } from "react-native";

class ServiceItem extends React.Component {
  constructor(elements) {
    // Constuire les éléments
    super(elements);

    service = this.props.service; // récupération de la props service
    _elements = []; // déclaration tableau
    _elements = service.elements; // récupération des éléments
    photo = _elements[0].value[0]; // récupération de la photo associée au service
  }
  
  render() {
    const {service, displayDetailForMember, affichResult} = this.props;
    return (
      <View style={styles.container}>
        <Image style={styles.image} source={{ uri: photo }} />
        <View style={styles.title_service}>
          <Text style={ styles.text_style }>{ service.title }</Text>

          <View style={styles.button_style}>
            <TouchableOpacity style={styles.Button} onPress={() => affichResult(service.title)}>
              <Image style={styles.image_inscription} source={require('../assets/icon/personne.png')} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.Button} onPress={() => displayDetailForMember(service.title, service.elements)}>
              <Image style={styles.image_inscription} source={require('../assets/icon/inscription.png')} />
            </TouchableOpacity>

          </View>
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    margin: 2,
    height: 160,
    backgroundColor: "#D3D3D3",
  },
  image: {
    margin: 5,
    height: 150,
    width: 150
  },
  image_inscription: {
    height: 60,
    width: 60,
    margin: 5
  },
  Button: {
    margin : 5,
    width: 80,
  },
  title_service: {
    flex: 1,
    flexWrap: "wrap", //Passer à la ligne si trop long
    alignItems: "center",
    flexDirection: "column",

  },
  button_style: {
    flexDirection: "row",
  },
  text_style: {
    fontWeight: 'bold',
    fontSize: 30,
    fontStyle: 'italic'
  }
});

export default ServiceItem;
