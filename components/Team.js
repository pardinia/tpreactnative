import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";

let nb_decalage = 10; //nombre de x pour deplacer accessoires
let test = -1; //test pour la rotation des accessoires
let zind = [0, 0, 0, 0]; //pour changer dynamiquement le zindex des vues

class Team extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: 0,
      arr2: [
        //tableau des accessoires
        { image: require("../assets/team/g-boucle.png"), left: -450 },
        { image: require("../assets/team/a-lunette_dodo.png"), left: 150 },
        { image: require("../assets/team/m-lunette.png"), left: 150 },
        { image: require("../assets/team/r-tacos.png"), left: -450 },
        { image: require("../assets/team/a-chien.png"), left: -450 },
        { image: require("../assets/team/g-lunette_soleil.png"), left: -450 },
        { image: require("../assets/team/m-boisson.png"), left: 150 },
        { image: require("../assets/team/r-ballon.png"), left: -450 },
        { image: require("../assets/team/m-chat.png"), left: 150 },
        { image: require("../assets/team/g-cafe.png"), left: -450 },
        { image: require("../assets/team/a-portable.png"), left: -450 },
        { image: require("../assets/team/r-skate.png"), left: -450 }
      ],
      rotations: [0, 0, 0, 0] //tableau pour les rotations
    };
  }

  // methode lancée a chaque mise en forme d un accessoire (creation et delacement)
  bouger(event) {
    switch (this.state.show) {
      case 0:
        zind = [20, 0, 0, 0];
        this.rightToLeft(event, 0);
        break;
      case 1:
        zind = [0, 20, 0, 0];
        this.leftToRight(event, 1);
        break;
      case 2:
        zind = [0, 0, 0, 20];
        this.leftToRight(event, 2);
        break;
      case 3:
        zind = [0, 0, 20, 0];
        this.rightToLeft(event, 3);
        break;
      case 4:
        zind = [0, 0, 0, 20];
        this.rightToLeft(event, 4);
        break;
      case 5:
        zind = [20, 0, 0, 0];
        this.rightToLeft(event, 5);
        break;
      case 6:
        zind = [0, 20, 0, 0];
        this.leftToRight(event, 6);
        break;
      case 7:
        zind = [0, 0, 20, 0];
        this.rightToLeft(event, 7);
        break;
      case 8:
        zind = [0, 20, 0, 0];
        this.leftToRight(event, 8);
        break;
      case 9:
        zind = [20, 0, 0, 0];
        this.rightToLeft(event, 9);
        break;
      case 10:
        zind = [0, 0, 0, 20];
        this.leftToRight(event, 10);
        break;
      case 11:
        zind = [0, 0, 20, 0];
        this.rightToLeft(event, 11);
        break;
      default:
        console.log("NUMBER NOT FOUND");
    }
  }

  //permet de deplacer de gauche a droite une image
  leftToRight(event, nb) {
    //on change d image quand l ancienne ne bouge plus
    if (event.nativeEvent.layout.x == this.state.x) {
      this.setState({
        show: this.state.show + 1
      });
    }
    //*2 car le changement n est pas effectif de suite
    if (event.nativeEvent.layout.x + nb_decalage * 2 < this.state.x) {
      this.state.arr2[nb].left = this.state.arr2[nb].left - nb_decalage;
      this.setState({
        arr2: this.state.arr2
      });
    }
    // pour le mettre a la bonne position a la fin
    else {
      this.state.arr2[nb].left = this.state.x;
      this.setState({
        arr2: this.state.arr2
      });
    }
  }

  //permet de deplacer de droite a gauche une image
  rightToLeft(event, nb) {
    //on change d image quand l ancienne ne bouge plus
    if (event.nativeEvent.layout.x == this.state.x) {
      this.setState({
        show: this.state.show + 1
      });
    }
    //*2 car le changement n est pas effectif de suite
    if (event.nativeEvent.layout.x - nb_decalage * 2 > this.state.x) {
      this.state.arr2[nb].left = this.state.arr2[nb].left + nb_decalage;
      this.setState({
        arr2: this.state.arr2
      });
    }
    // pour le mettre a la bonne position a la fin
    else {
      this.state.arr2[nb].left = this.state.x;
      this.setState({
        arr2: this.state.arr2
      });
    }
  }

  //pour lancer le timer pour la rotation
  componentDidMount() {
    this.lancerrot();
  }

  //lancer la rotation si tous les accessoires sont apparu
  lancerrot() {
    this.interval2 = setInterval(() => {
      if (this.state.show == 12) {
        test = test * -1;
        this.rotation(test);
      }
    }, 5000);
  }

  //effectu la rotation
  rotation(nb) {
    if (nb == 1) {
      this.interval = setInterval(() => {
        this.setState({
          rotations: this.state.rotations.map(item => item + Math.random() * 20)
        });
      }, 0);
    } else {
      clearInterval(this.interval); //on stop la rotation
      this.setState({
        rotations: this.state.rotations.map(item => 0) //on met de face
      });
    }
  }

  // recupere la taille et la pos d'un bonhomme
  eventAvatar(event) {
    this.setState({
      height: event.nativeEvent.layout.height,
      x: event.nativeEvent.layout.x,
      y: event.nativeEvent.layout.y
    });
  }

  //les accessoires ajoutes dynamiquement
  img(nb) {
    return (
      <Image
        style={{
          position: "absolute",
          height: this.state.height,
          right: this.state.arr2[nb].left
        }}
        onLayout={event => this.bouger(event)}
        resizeMode={"contain"}
        source={this.state.arr2[nb].image}
      />
    );
  }

  render() {
    return (
      <View style={styles.team}>
        <View style={styles.ligne}>
          <View
            style={[
              styles.personnage,
              {
                zIndex: zind[0],
                transform: [{ rotateY: this.state.rotations[0] + "deg" }]
              }
            ]}
          >
            <Image
              style={styles.image}
              resizeMode={"contain"}
              onLayout={event => this.eventAvatar(event)}
              source={require("../assets/team/guillaume.png")}
            />
            {this.state.show >= 0 ? this.img(0) : null}
            {this.state.show >= 5 ? this.img(5) : null}
            {this.state.show >= 9 ? this.img(9) : null}

            <Text>Guillaume</Text>
          </View>
          <View
            style={[
              styles.personnage,
              {
                zIndex: zind[1],
                transform: [{ rotateY: this.state.rotations[1] + "deg" }]
              }
            ]}
          >
            <Image
              style={styles.image}
              resizeMode={"contain"}
              source={require("../assets/team/marc.png")}
            />
            {this.state.show >= 2 ? this.img(2) : null}
            {this.state.show >= 6 ? this.img(6) : null}
            {this.state.show >= 8 ? this.img(8) : null}

            <Text>Marc-Antoine</Text>
          </View>
        </View>

        <View style={styles.ligne}>
          <View
            style={[
              styles.personnage,
              {
                zIndex: zind[2],
                transform: [{ rotateY: this.state.rotations[2] + "deg" }]
              }
            ]}
          >
            <Image
              style={styles.image}
              resizeMode={"contain"}
              source={require("../assets/team/raphael.png")}
            />
            {this.state.show >= 3 ? this.img(3) : null}
            {this.state.show >= 7 ? this.img(7) : null}
            {this.state.show >= 11 ? this.img(11) : null}

            <Text>Raphael</Text>
          </View>

          <View
            style={[
              styles.personnage,
              {
                zIndex: zind[3],
                transform: [{ rotateY: this.state.rotations[3] + "deg" }]
              }
            ]}
          >
            <Image
              style={styles.image}
              resizeMode={"contain"}
              source={require("../assets/team/anthony.png")}
            />
            {this.state.show >= 1 ? this.img(1) : null}
            {this.state.show >= 4 ? this.img(4) : null}
            {this.state.show >= 10 ? this.img(10) : null}

            <Text>Anthony</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  team: {
    flex: 1,
    alignItems: "center"
  },
  ligne: {
    flex: 0.5,
    flexDirection: "row"
  },
  personnage: {
    flex: 0.5,
    alignItems: "center",
    flexDirection: "column"
  },
  image: {
    flex: 0.8
  }
});

export default Team;
